import os
import hashlib
import time

from flask import *
from flask_sqlalchemy import SQLAlchemy

port = int(os.getenv("PORT", "9000"))
musho_url = os.getenv("MUSHO_URL", "http://localhost:%d" % port)
debug = os.getenv("DEBUG", None)
database_url = os.getenv("DATABASE_URL", 'sqlite:////tmp/musho.db')

print("DATABASE_URL:\t\t%s" % database_url)
print("MUSHO_URL:\t\t%s" % musho_url)
print("PORT:\t\t\t%d" % port)
print("DEBUG:\t\t\t%s" % debug)

musho = Flask(__name__)
musho.config['SQLALCHEMY_DATABASE_URI'] = database_url
# musho.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# musho.config['SQLALCHEMY_ECHO'] = True
# musho.config['SQLALCHEMY_POOL_SIZE'] = int(os.getenv("POOL_SIZE", "16"))


db = SQLAlchemy(musho)
# db.create_all()

class Link(db.Model):
    __tablename__ = "links"
    id = db.Column(db.Integer, primary_key=True)
    short_url = db.Column(db.String(), unique=True)
    full_url = db.Column(db.String(), unique=True)
    
    def __init__(self, short, full):
        self.full_url = full
        self.short_url = short
    
    def __repr__(self):
        return "<Link '%s' -> '%s'" % (self.short_url, self.full_url)


@musho.route("/")
def index() :
    return render_template("index.html")

@musho.route("/save", methods=['POST'])
def saveLink() :
    now = time.strftime("%d-%b-%Y %H:%m")
    remote_ip = request.remote_addr
    url = request.form["url"]
    hash = hashlib.new('sha1') 
    hash.update(url.encode('ascii','ignore'))
    digest = hash.hexdigest()
    short_url = digest[0:8]
    new_link = Link(short_url, url)
    ## check if already present
    present = Link.query.filter_by(full_url=url).first()
    if present is None :
        db.session.add(new_link)
        db.session.commit()
    full_url = musho_url +"/l/" + short_url
    print("Sortened %s -- %s -- %s " % (short_url, remote_ip, now) )
    return render_template('shortened_link.html', url=full_url)


@musho.route("/l/<string:short_url>")
def resolveLink(short_url):
    if debug:
        print("Risolvo il link '%s'" % short_url)
    target = Link.query.filter_by(short_url=short_url).first()
    if target is not None :
        return redirect(target.full_url)
    else :
        return redirect("/")

@musho.route('/static/<path:path>')
def static_proxy(path):
  # send_static_file will guess the correct MIME type
    println("Got request for path: " + path)
    return musho.send_static_file(path)


if __name__ == "__main__" :
    if debug :
        musho.debug = True
    musho.run(port=port)
