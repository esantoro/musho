#!/usr/bin/env bash

export DATABASE_URL=postgresql://manu:manu@127.0.0.1:5432/musho
export DEBUG=true
export PORT=9000
export MUSHO_URL=http://localhost:$PORT
# export MUSHO_URL=http://musho.tk/
