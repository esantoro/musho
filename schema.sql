--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: links; Type: TABLE; Schema: public; Owner: manu; Tablespace: 
--

CREATE TABLE links (
    id integer NOT NULL,
    short_url character varying,
    full_url character varying
);


ALTER TABLE public.links OWNER TO manu;

--
-- Name: links_id_seq; Type: SEQUENCE; Schema: public; Owner: manu
--

CREATE SEQUENCE links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.links_id_seq OWNER TO manu;

--
-- Name: links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: manu
--

ALTER SEQUENCE links_id_seq OWNED BY links.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: manu
--

ALTER TABLE ONLY links ALTER COLUMN id SET DEFAULT nextval('links_id_seq'::regclass);


--
-- Data for Name: links; Type: TABLE DATA; Schema: public; Owner: manu
--

COPY links (id, short_url, full_url) FROM stdin;
\.


--
-- Name: links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: manu
--

SELECT pg_catalog.setval('links_id_seq', 1, false);


--
-- Name: links_full_url_key; Type: CONSTRAINT; Schema: public; Owner: manu; Tablespace: 
--

ALTER TABLE ONLY links
    ADD CONSTRAINT links_full_url_key UNIQUE (full_url);


--
-- Name: links_pkey; Type: CONSTRAINT; Schema: public; Owner: manu; Tablespace: 
--

ALTER TABLE ONLY links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: links_short_url_key; Type: CONSTRAINT; Schema: public; Owner: manu; Tablespace: 
--

ALTER TABLE ONLY links
    ADD CONSTRAINT links_short_url_key UNIQUE (short_url);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

