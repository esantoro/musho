Musho
========

Musho is the **M**icro **u**rl **sho**rtener.

It's a very very basic web application built as a demo of what you can
do in terms of web development with Python.

Of course, you can do much more than this, but this is meant to be a
super-quick demo, as short as possible, as fast as it can be.

## Running on your desktop

In order to be able to run this application on the Heroku public
cloud, we use some particular programming tecniques, like setting some
options via environment variables.

For this reason, there is a `ENV_SAMPLE.sh` script in the repository.

Such file is meant to be copied as (for example) `DEV_ENV.sh` on the
development machine and then "sourced" before running the application:

    cp ENV_SAMPLE.sh DEV_ENV.sh
	vi DEV_ENV.sh ## customize your settings
	source DEV_ENV.sh
	python musho.py
	

## Running on a Dokku instance

See the `DOKKU_DEPLOY.md` file.
