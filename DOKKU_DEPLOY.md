Notes for Deployment on dokku
-----------------------------

Here are some notes for a quick deployment on a dokku instance

I am assuming all commands are issued as the root user on the dokku instance

1. `dokku plugin:install https://github.com/dokku/dokku-postgres.git postgres`
2. `dokku postgres:create musho_db`
3. (from the source code repository) `ssh root@musho.tk dokku postgres:connect musho_db < schema.sql`
4. dokku apps:create musho.tk
5. dokku postgres:link musho_db musho.tk
6. dokku config:set musho.tk MUSHO_URL=http://musho.tk

Deploy:

1. git remote add dokku dokku@musho.tk:musho.tk

